import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import thunk from 'redux-thunk';
import { ProductsReducer } from './products/productsReducer';

let composeEnhancers;

// @ts-ignore
switch (process.env.NODE_ENV) {
    case 'production':
        composeEnhancers = compose;
        break;
    case 'development':
        // @ts-ignore
        composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
        break;
    default:
        composeEnhancers = compose;
}

const rootReducer = combineReducers({
    products: ProductsReducer
})

export const store = createStore(
    rootReducer, 
    composeEnhancers(applyMiddleware(thunk))
);