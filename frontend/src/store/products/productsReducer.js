import { GET_PRODUCTS } from "./productActions"

const initialState = {
	products: []
}
export const ProductsReducer = (state = initialState, {type, payload}) => {
	switch (type) {
		case GET_PRODUCTS:
			return {
				...state.products
			}
		default: 
			return state
	}
}