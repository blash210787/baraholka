import React from "react";
import { Wrapper } from "../common/wrapper/Wrapper";

export const TestComponent = () => {
    return (
        <Wrapper>
            <div>
                <h1>H1 тест компонент</h1>

                <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eos rem, quibusdam autem magni officiis, maiores, doloremque iusto nulla recusandae pariatur laborum enim cupiditate consectetur exercitationem ad deleniti iure quae incidunt.</p>

                <a href="/#">fsdfdfsdf</a>
                <ul>
                    <li>fsdfs</li>
                    <li>fsdfs</li>
                    <li>fsdfs</li>
                    <li>fsdfs</li>
                </ul>
            </div>
        </Wrapper>
    )
}