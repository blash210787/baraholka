import React from "react";
// @ts-ignore
import style from './wrapper.module.scss';

export const Wrapper = ({children}) => {
    return (
        <div className={style.wrapperContainer}>
            {children}
        </div>
    );
};