import React from 'react';
import { Link } from "react-router-dom";
import { getHomeLink, getAdsLink, getAdLink, getProfileLink } from "../../routes";

export const Header = () => {
    const navMenu = [{
            id: Date.now(),
            path: getHomeLink(),
            title: 'Главная',
        },
        {
            id: Date.now(),
            path: getProfileLink(),
            title: 'Профиль',
        },
        {
            id: Date.now(),
            path: getAdsLink(),
            title: 'Объявления',
        },
        {
            id: Date.now(),
            path: getAdLink(),
            title: 'Объявление',
        },

    ];

    return (
        <header>
            <div>
                <Link to={navMenu[0].path}>obmenRU</Link>
                <div>
                    {
                        navMenu?.map((value, index) => (
                            <Link to={navMenu[index].path} key={navMenu[index].id + index}>{navMenu[index].title}</Link>
                        ))
                    }
                </div>
            </div>
        </header>
    );
}