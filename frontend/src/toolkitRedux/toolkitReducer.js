import { createAction, createReducer } from "@reduxjs/toolkit";


const initialSatte = {
    count: 5
}

export const increment = createAction('INCREMENT');

export default createReducer(initialSatte, {
    [increment]: (state) => {
        state.count = state.count + 1
    }
})