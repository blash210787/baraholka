import { createSlice } from "@reduxjs/toolkit";


const toolkitSlice = createSlice({
    name: "toolkit",
    initialState: {
        count: 10,
        tasks: [
            {id: 1, name: 'задача 1'}
        ],
    },
    reducers: {
        increment(state) {
            state.count = state.count + 1
        },
        decrement(state) {
            state.count = state.count - 1
        },
        addTask(state, action) {
            state.tasks.push(action.payload)
        }
    }
});

export default toolkitSlice.reducer;
export const {increment, decrement, addTask} = toolkitSlice.actions;