import { Container } from "./components/common/container/Container";
import { Routes, Route } from 'react-router-dom';
import { getHomeLink, getAdLink, getAdsLink, getProfileLink } from "./routes";
import { Layout, Profile, Home, Ad, Ads, NotFound} from './pages';

function App() {
    return (
        <div className="App">
            <Container>
                <Routes>
                    <Route path={getHomeLink()} element={<Layout/>}>
                        <Route path={getHomeLink()} element={<Home/>}/>
                        <Route path={getAdsLink()} element={<Ads/>}/>
                        <Route path={getAdLink()} element={<Ad/>}/>
                        <Route path={getProfileLink()} element={<Profile/>}/>
                        <Route path='*' element={<NotFound/>}/>
                    </Route>
                </Routes>
            </Container>
        </div>
    );
}

export default App;
