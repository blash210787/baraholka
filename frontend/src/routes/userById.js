import { getUserLink } from './user';
export const getUserByIdLink = (userId = ':userId') => [getUserLink(), userId].join('/');