import { getAdsLink } from './ads';
export const getAdLink = (adId = ':adId') => [getAdsLink(), adId].join('/');