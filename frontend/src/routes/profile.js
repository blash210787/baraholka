import { getUserByIdLink } from './userById';
export const getProfileLink = () => ['/profile', getUserByIdLink()].join('');
