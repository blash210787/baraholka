import { stringifyUrl } from 'query-string';


export const getAPIEndpoint = () => process.env.REACT_APP_API_URL;

export const getAuthToken = () => [getAPIEndpoint(), 'auth', 'token'].join('/');

export const getRefreshAuthToken = () => [getAuthToken(), 'refresh'].join('/');

// Pagination endpoints

export const getPaginationParams = (page, page_size = 10) => (url) => stringifyUrl({
    url,
    query: {
        page,
        page_size
    }
});

// ADS endpoints - 'ads': объявления

export const getAdsPath = () => [getAPIEndpoint(), 'ads'].join('/');

export const addNewAdPath = () => [getAdsPath(), 'add'].join('/');

export const getAdByIdPath = (adId) => [getAdsPath(), 'view', adId].join('/');

export const patchAdByIdPath = (adId) => [getAdsPath(), 'edit', adId].join('/'); // частичное изменение данных

// Users endpoints

export const getUsersPath = () => [getAPIEndpoint(), 'users'].join('/');

export const addUserPath = () => [getUsersPath(), 'add'].join('/');

export const getUserByIdPath = (userId) => [getUsersPath(), 'view', userId].join('/');

export const patchUserByIdPath = (userId) => [getUsersPath(), 'edit', userId].join('/'); // частичное изменение данных