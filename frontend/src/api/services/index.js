import {
    getAuthToken,
    getRefreshAuthToken,
    getPaginationParams,
    getAdsPath,
    addNewAdPath,
    getAdByIdPath,
    patchAdByIdPath,
    addUserPath,
    getUserByIdPath,
    patchUserByIdPath,
} from "../../api/endpoints";

import {
    executeGetAuth,
    executeGetRefreshToken,
    executeGetAds,
    executeAddNewAd,
    executeGetAdById,
    executePatchAdByIdPath,
    executeAddUser,
    executeGetUserById,
    executePatchUserDataById,
} from "../../api/services/performers";


export const api = {
    getToken: async (authData) => executeGetAuth(getAuthToken(), authData),
    getRefreshToken: async () => executeGetRefreshToken(getRefreshAuthToken()),

    getAds: async (page, page_size, token) => executeGetAds(getPaginationParams(page, page_size)(getAdsPath()), token),
    addAd: async (newAdData, token) => executeAddNewAd(addNewAdPath(), newAdData, token),
    getAdById: async (adId, token) => executeGetAdById(getAdByIdPath(adId), token),
    patchAdById: async (adId, updatedAdData, token) => executePatchAdByIdPath(patchAdByIdPath(adId), updatedAdData, token),

    addUser: async (userData, token) => executeAddUser(addUserPath(), userData, token),
    getUserById: async (userId, token) => executeGetUserById(getUserByIdPath(userId), token),
    patchUserDataById: async (userId, newUserData, token) => executePatchUserDataById(patchUserByIdPath(userId), newUserData, token),
}