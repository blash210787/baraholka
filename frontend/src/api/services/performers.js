// AUTH 5
// ADS 66
// USERS 139

//AUTH
export const executeGetAuth = async (url, authData) => {

    const res = await fetch(url, {
        method: "POST",
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(authData),
    });
    if (!res.ok) {

        sessionStorage.removeItem('accessTokenData');
        localStorage.removeItem('refreshTokenData');

        res.json().then(body => console.log(body));
        console.log('Error status =', res.status);

        return res.status;
    }
    const tokens = await res.json();

    const accessData = {
        access: tokens.access,
        access_lifetime: tokens.access_lifetime
    };
    const refreshData = {
        refresh: tokens.refresh,
        refresh_lifetime: tokens.refresh_lifetime
    };

    sessionStorage.setItem('accessTokenData', JSON.stringify(accessData));
    localStorage.setItem('refreshTokenData', JSON.stringify(refreshData));
}

export const executeGetRefreshToken = async (url) => {

    const res = await fetch(url, {
        method: "POST",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "refresh": `${JSON.parse(localStorage.refreshTokenData).refresh}`,
        }),
    });
    if (!res.ok) {
        res.json().then(body => console.log(body));
        console.log('Error status =', res.status);

        return res.status;
    }

    const tokens = await res.json();

    sessionStorage.setItem('accessTokenData', JSON.stringify(tokens));
    console.log("accessTokenData успешно обновлен в sessionStorage", tokens);

    return JSON.parse(sessionStorage.accessTokenData).access;
}

// ADS

export const executeGetAds = async (url, token) => {

    const res = await fetch(url, {
        method: "GET",
        headers: {
            'Authorization': `Bearer ${token}`
        }
    })
    if (!res.ok) {
        res.json().then(body => console.log(body));
        console.log('Error status =', res.status);
        return res.status;
    }
    const ads = await res.json();
    return ads;
}

export const executeAddNewAd = async (url, newAdData, token) => {
    const res = await fetch(url, {
        method: "POST",
        headers: {
            'Authorization': `Bearer ${token}`,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(newAdData),
    })
    if (!res.ok) {
        res.json().then(body => console.log(body));
        console.log('Error status =', res.body);
        return res.status;
    }
    const newAdDataRes = await res.json();
    return newAdDataRes;
}

export const executeGetAdById = async (url, token) => {

    const res = await fetch(url, {
        method: "GET",
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });
    if (!res.ok) {
        res.json().then(body => console.log(body));
        console.log('Error status =', res.status);
        return res.status;
    }
    const adByIdData = await res.json();
    return adByIdData;
}

export const executePatchAdByIdPath = async (url, updatedAdData, token) => {

    const res = await fetch(url, {
        method: "PATCH",
        headers: {
            'Authorization': `Bearer ${token}`,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(updatedAdData),
    });
    if (!res.ok) {
        res.json().then(body => console.log(body));
        console.log('Error status =', res.status);
        return res.status;
    }
    const updatedAdDataById = await res.json();
    return updatedAdDataById;
}

// USERS

export const executeAddUser = async (url, userData, token) => {
    const res = await fetch(url, {
        method: "POST",
        headers: {
            'Authorization': `Bearer ${token}`,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(userData),
    })
    if (!res.ok) {
        res.json().then(body => console.log(body));
        console.log('Error status =', res.status);
        return res.status;
    }
    const newUserData = await res.json();
    return newUserData;
}

export const executeGetUserById = async (url, token) => {
    const res = await fetch(url, {
        method: "GET",
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });
    if (!res.ok) {
        res.json().then(body => console.log(body));
        console.log('Error status =', res.status);
        return res.status;
    }
    const userById = await res.json();
    return userById;
}

export const executePatchUserDataById = async (url, newUserData, token) => {
    const res = await fetch(url, {
        method: "PATCH",
        headers: {
            'Authorization': `Bearer ${token}`,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(newUserData),
    });
    if (!res.ok) {
        res.json().then(body => console.log(body));
        console.log('Error status =', res.status);
        return res.status;
    }
    const newUserDataById = await res.json();
    return newUserDataById;
}