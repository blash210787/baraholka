import React from "react";
import { useDispatch, useSelector } from "react-redux";
import {increment, decrement} from '../../toolkitRedux/toolkitSlice';

export const Profile = () => {
    const dispatch = useDispatch();
    const count = useSelector(state => state.toolkit.count);
    const tasks = useSelector(state => state.toolkit.tasks);


    console.log('count', count);
	return (
        <div>
            <h1>Profile1</h1>

            <br />

            <button onClick={() => dispatch(increment())}>increment</button>
            <button onClick={() => dispatch(decrement())}>decrement</button>

            {count}


            <br />

            <h3>список задач</h3>

            {tasks.map(task => (
                <div>{task.name}</div>
            ))}
        </div>
	);
};