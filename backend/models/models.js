const sequelize = require('../db');
const {DataTypes} = require('sequelize');

const db = {
	products: [
		{id: 1, name: 'Доска'},
		{id: 2, name: 'Кружка'},
		{id: 3, name: 'Монитор'},
		{id: 4, name: 'Ноутбук'},
		{id: 4, name: 'Резак по дереву'},
	]
}

const User = sequelize.define('Users', {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
	firstname: {type: DataTypes.STRING},
	lastname: {type: DataTypes.STRING},
    email: {type: DataTypes.STRING, unique: true},
    password: {type: DataTypes.STRING},
    role: {type: DataTypes.STRING, defaultValue: "USER"},
	tel: {type: DataTypes.STRING, unique: true},
});

module.exports = {
	User,
	db
}