'use strict';
const {faker} = require('@faker-js/faker');

module.exports = {

    up: (queryInterface, Sequelize) => {
        let newData = [];
        for (let i=0; i<10; i++){
            newData.push({
                firstName: faker.internet.userName(),
                lastName: faker.internet.userName(),
                email: faker.internet.email(),
                password: faker.internet.password(),
                tel: faker.phone.phoneNumber(),
                createdAt: new Date(),
                updatedAt: new Date()
            });
        }
        console.log(newData)
        return queryInterface.bulkInsert('Users', newData);

    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('Users', null, {});
    }
};
