# baraholka

Финальный проект "baraholka".

Ветка бэка.

## config server

PORT: 7000

## запуск проект

>npm start

## endpoints

В корне проекта: .swagger.MD

## DB migrations & seeds

### Запуск миграций командой:

>npx sequelize-cli db:migrate

- создает таблицы в БД указанные в моделях </br>

### Запуск сидов командой:

>npx sequelize-cli db:seed:all

- заполняет таблицы в БД тестовыми данными </br>
  см. [Документацию](https://sequelize.org/docs/v6/other-topics/migrations/)