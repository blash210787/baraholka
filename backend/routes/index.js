const express = require('express');
const router = express();
const testApi = require('./testAPI');
const productsAPI = require('./productsAPI');
const authMiddleware = require('../middleware/AuthMiddleware');

const {
	registrationController,
	authorizationController,
	checkController
} = require('../controllers/authController');

router.use('/test', testApi);
router.use('/products', productsAPI);

router.post('/registration', registrationController);
router.post('/login', authorizationController);
router.get('/auth', authMiddleware, checkController);

module.exports = router;