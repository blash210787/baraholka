const express = require('express');
const app = express();

const productsController = require('../controllers/productsController');

app.get('/', (req, res) => {
	res.send(productsController())
})

module.exports = app;