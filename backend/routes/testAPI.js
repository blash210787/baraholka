const express = require('express');
const app = express();

const testApiController = require('../controllers/testApiController');

app.get('/', async (req, res) => {
	let result = await testApiController();

	console.log('result', result);
	res.send(result)
})

app.post('/', (req, res) => {
	res.send({
		status: 200,
		message: 'post запрос отправил ответ'
	})
})

module.exports = app;