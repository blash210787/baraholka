const express = require('express');
const path = require('path');
require('dotenv').config();
const sequelize = require('./db');

const PORT = process.env.PORT || 7000;
const app = express();
const router = require('./routes/index');


//const User = require('./models/user');

app.use(express.json());
app.use(express.static(path.resolve(__dirname, 'static')));
app.use('/api', router);

const start = async () => {
    try {
        app.listen(PORT, () => {
            console.log('backend start on ' + PORT + ' port...');
        })

		sequelize.authenticate();
		sequelize.sync();
    } catch (e) {
        console.log('error', e);
    }
}

start();
//User.findAll().then((result) => {
//	console.log(result)
//})