const ApiError = require('../error/ApiError');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const { User } = require("../models/models");

const generatejwt = (id, email, role) => {
	return jwt.sign({
			id,
			email,
			role
		},
		process.env.SECRET_KEY, {
			expiresIn: process.env.TOKEN_EXPIRES_IN
		}
	);
}


const authorizationController = async (req, res, next) => {
	const {
		email,
		password
	} = req.body;

	const user = await User.findOne({
		where: {
			email
		}
	});

	if (!user) {
		res.send({
			status: "error",
			message: "Пользователь не найден"
		});

		return next(ApiError.internal('Пользователь не найден'));
	}

	let comparePassword = bcrypt.compareSync(password, user.password);

	if (!comparePassword) {
		res.send({
			status: "error",
			message: "Не верный пароль!"
		});

		return next(ApiError.internal('Не верный пароль!'));
	}

	const token = generatejwt(user.id, user.email);

	return res.json({
		status: 200,
		token
	});
}

const registrationController = async (req, res, next) => {
	const {
		email,
		password,
		role,
		firstname,
		lastname
	} = req.body;

	if (!email || !password) {
		res.send({
			status: "error",
			message: "Некорректный пароль или email"
		});

		return next(ApiError.badRequest('Некорректный пароль или email'));
	}

	try {
		const candidate = await User.findOne({
			where: {
				email
			}
		});

		if (candidate) {
			res.send({
				status: "error",
				message: "Такой email существует"
			});

			return next(ApiError.badRequest('Такой email существует'));
		}

		const hashPassword = await bcrypt.hash(password, 5);
		const user = await User.create({
			email,
			password: hashPassword,
			role,
			firstname,
			lastname
		});
		const token = generatejwt(user.id, user.email, user.role);

		return res.json({
			status: 200,
			token
		});

	} catch (e) {
		console.log('error', e);
		res.send({
			error: "ошибка"
		})
	}

}

const checkController = (req, res, next) => {
	const token = generatejwt(req.user.id, req.user.email, req.user.role);

	return res.json({
		status: 200,
		token
	});
}

module.exports = {
	authorizationController,
	registrationController,
	checkController
}