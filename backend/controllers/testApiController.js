const testApiController = () => {
	const data = {
		status: 200,
		message: 'данные из TestApiController'
	}
	data.createdAt = new Date();

	return data
}

module.exports = testApiController;