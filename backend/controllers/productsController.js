const db = require('../models/models');

const productsController = () => {

	const products = db.products;
	const data = {
		status: 200,
		message: 'данные из productsController',
		products
	}
	data.createdAt = new Date();

	return data 
}

module.exports = productsController;